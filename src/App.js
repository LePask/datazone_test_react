import logo from './logo.svg';
import './App.css';
import DropdownContainer from './components/DropdownContainer';

function App() {
  return (
    <div className="App">
      <DropdownContainer />  
    </div>
  );
}

export default App;
