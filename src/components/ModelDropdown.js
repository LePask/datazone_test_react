import React, { useState, useEffect } from "react";
import { BASE_URL, MODEL_ENDPOINT, JSON_FORMAT } from "../utils/constants";

function returnModelTextOrLoading(isLoading){
    if (isLoading === true) {
        return "Loading...";
    } else {
        return "3 | Model";
    }
}

const ModelDropdown = (props) => {
  const { selectedMake, selectedModel, handleModel } = props;
  const [models, setModels] = useState([]);
  const [isLoading, setIsLoading] = useState();

  const finalUrl = `${BASE_URL}${MODEL_ENDPOINT}${selectedMake}?${JSON_FORMAT}`;

  useEffect(() => {
    const fetchData= async () => {
      try {
        setIsLoading(true);
        const response = await fetch(finalUrl);
        const jsonData = await response.json();
        setModels(jsonData.Results);
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        console.log('Error! ', error);
      }
    };
    if (selectedMake !== "") {
      fetchData();
    }
  }, [selectedMake]);

  return (
    <div className="vehicle-dropdown">
        <label htmlFor="model">Select model</label>
      <select id="model" value={selectedModel} onChange={handleModel} disabled={!selectedMake}>
        <option value=""> {returnModelTextOrLoading(isLoading)} </option>
        {models.map((model) => (
          <option key={model.Model_ID} value={model.Model_Name}>
            {model.Model_Name}
          </option>
        ))}
      </select>
    </div>
  );
};

export default ModelDropdown;

