import React, { useState, useEffect } from "react";
import { filteredMakeItems } from "../utils/dropdownUtils";
import { BASE_URL, MAKES_ENDPOINT, JSON_FORMAT } from "../utils/constants";

function returnMakeTextOrLoading(isLoading){
  if (isLoading === true){
      return "Loading...";
  } else {
      return "2 | Make";
  }
}

const MakeDropdown = (props) => {
  const { selectedYear, selectedMake, handleMake } = props;
  const [makes, setMakes] = useState([]);
  const [isLoading, setIsLoading] = useState();

  const finalUrl = `${BASE_URL}${MAKES_ENDPOINT}${selectedYear}${JSON_FORMAT}`;

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const response = await fetch(finalUrl);
        const jsonData = await response.json();
        setMakes(jsonData.Results);
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        console.log('Error! ', error)
      }
    };
    if (selectedYear !== "") {
      fetchData();
    }
  }, [selectedYear]);

  const filteredMakes = makes?.length > 0 ? 
    filteredMakeItems(makes) :
    undefined;

  return (
    <div className="vehicle-dropdown">
      <label htmlFor="make">Select make</label>
      <select id="make" value={selectedMake} onChange={handleMake} disabled={!selectedYear}>
        <option value=""> {returnMakeTextOrLoading(isLoading)} </option>
        {filteredMakes}
      </select>
    </div>
  );
};

export default MakeDropdown;
