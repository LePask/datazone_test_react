import React from 'react';

function DropdownButton(props) {
  const { selectedYear, handleYear } = props;

  const yearOptions = Array.from({length: 29}, (_, i) => 1995 + i).map((year) => (
    <option key={year} value={year}>{year}</option>
  ));

  // If there is no year selected at begining, set the firts value
  const settedYear = selectedYear === "" ?
    yearOptions[0] :
    selectedYear;

  return (
    <div className="vehicle-dropdown">
      <label htmlFor="year">Select a year</label>
      <select id="year" value={settedYear} onChange={handleYear}>
        <option value=""> 1 | Year</option>
        {yearOptions}
      </select>
    </div>
  );
}

export default DropdownButton;