
import MakeDropdown from './MakeDropdown';
import YearDropdown from './YearDropdown';
import ModelDropdown from './ModelDropdown';
import { useState } from 'react';
import '../styles/VehicleDropdowns.css'

function DropdownContainer() {

  const [vehicleInfo, setVehicleInfo] = useState({
    selectedYear: "",
    selectedMake: "",
    selectedModel: "",
  });

  function handleYear(event) {
    setVehicleInfo(prevState => ({ 
        ...prevState, 
        selectedYear: event.target.value,
        selectedMake: "",
        selectedModel: "",
    }));
  }

  function handleMake(event){
    setVehicleInfo(prevState => ({ 
        ...prevState, 
        selectedMake: event.target.value,
        selectedModel: "", 
    }));
  }

  function handleModel(event){
    setVehicleInfo(prevState => ({ 
        ...prevState, 
        selectedModel: event.target.value 
    }));
  }

  return (    
    <div className="vehicle-dropdowns-container">
        <div className="vehicle-dropdowns">
            <div className="vehicle-text-container">
                <p className="set-vehicle-text">SET YOUR VEHICLE</p>
                <p className="exact-fit-text">Get an exact fit for your vehicle.</p>
            </div>
            <YearDropdown 
                selectedYear={vehicleInfo.selectedYear} 
                handleYear={handleYear} 
            />
            <MakeDropdown 
                selectedYear={vehicleInfo.selectedYear}
                selectedMake={vehicleInfo.selectedMake}
                handleMake={handleMake} 
            />
            <ModelDropdown 
                selectedMake={vehicleInfo.selectedMake}
                selectedModel={vehicleInfo.selectedModel}
                handleModel={handleModel}
            />
        </div>
    </div>
  );
}

export default DropdownContainer;
