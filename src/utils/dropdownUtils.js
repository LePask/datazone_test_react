// This function saves the makes in a set to avoid elements duplicated
export function filteredMakeItems (arrayOfMakes) {
    const mySet = new Set();
    for(const make of arrayOfMakes){
        if (!mySet.has(make.MakeName)){
            mySet.add(make.MakeName)
        }
    }
    const makeItems = Array.from(mySet).map((make) => (
        <option key={make} value={make}>
          {make}
        </option>
    )); 
    return makeItems;
};
