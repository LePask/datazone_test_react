export const BASE_URL = "https://vpic.nhtsa.dot.gov/api/";
export const MAKES_ENDPOINT = "vehicles/GetMakesForManufacturerAndYear/mer?year=";
export const MODEL_ENDPOINT = "vehicles/getmodelsformake/";
export const JSON_FORMAT = "&format=json";